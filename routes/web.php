<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get("install", "ShopifyController@install");
Route::post("uninstall", "ShopifyController@uninstall");
Route::get("auth","ShopifyController@auth");


Auth::routes(['register' => false]);

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:admin']], function() {
Route::resource('configs', 'ConfigController');
Route::resource('users', 'UserController');
});








// GDPR Routes

/**
 * When a store owner requests deletion of data on behalf of a customer
 * If your app has been granted access to the store's customers or orders, 
 * then you receive a redaction request webhook with the resource IDs that you need to 
 * redact or delete. 
 * In some cases, a customer record contains only the customer's email address
 */
 Route::post('customers/redact', 'RedactController@customers');
 /**
  * 48 hours after a store owner uninstalls your app, 
  * Shopify sends you a shop/redact webhook. 
  * This webhook provides the store's shop_id and shop_domain 
  * so that you can erase the customer information for that store from your database.
  */
 Route::post('shop/redact', 'RedactController@shop');
 
 /**
  * When a customer requests their data from a store owner, 
  * Shopify sends a payload on the customers/data_request topic 
  * to the apps installed on that store
  * It's your responsibility to provide this data to the store owner directly
  */
 Route::post('customers/data_request', 'RedactController@dataRequest');