<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Shopify Api
    |--------------------------------------------------------------------------
    |
    | This file is for setting the credentials for shopify api key and secret.
    |
    */
   
    'key' => env("SHOPIFY_API_KEY", ""),
    'secret' => env("SHOPIFY_SECRET", ""),
    'app_slug' => env("APP_SLUG", env('APP_NAME')),
    'redirect_uri' => env("REDIRECT_URI"),
    'app_base_url' => env("APP_BASE_URL"),
    'uninstall_webhook_address' => env("UNINSTALL_WEBHOOK"),
    'theme_name' => env('THEME_NAME'),
    'scopes' => explode(',', env("SCOPES"))
    
];