<div class="table-responsive">
    <table class="table" id="configs-table">
        <thead>
            <tr>
                <th>Name</th>
        <th>Type</th>
        <th>Configuration</th>
        <th>Status</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($configs as $config)
            <tr>
                <td>{{ $config->name }}</td>
            <td>{{ $config->type }}</td>
            <td>{{ $config->configuration }}</td>
            <td>{{ $config->status }}</td>
                <td>
                    {!! Form::open(['route' => ['configs.destroy', $config->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('configs.show', [$config->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('configs.edit', [$config->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
{{ $configs->render() }}
