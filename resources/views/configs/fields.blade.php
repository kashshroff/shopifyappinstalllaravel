<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Type Field -->
<div class="form-group col-sm-6">
    {!! Form::label('type', 'Type:') !!}
    {!! Form::select('type', ['Integer' => 'Integer', 'String' => 'String', 'Array' => 'Array', 'JSON' => 'JSON', 'Float' => 'Float'], null, ['class' => 'form-control']) !!}
</div>

<!-- Configuration Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('configuration', 'Configuration:') !!}
    {!! Form::textarea('configuration', null, ['class' => 'form-control']) !!}
</div>

<!-- Status Field -->
<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status:') !!}
    <label class="checkbox-inline">
        {!! Form::hidden('status', 0) !!}
        {!! Form::checkbox('status', '1', null) !!}
    </label>
</div>


<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('configs.index') }}" class="btn btn-default">Cancel</a>
</div>
