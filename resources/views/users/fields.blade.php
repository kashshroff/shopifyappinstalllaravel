<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Email Field -->
<div class="form-group col-sm-6">
    {!! Form::label('email', 'Email:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<!-- Locations At Field -->
<div class="form-group col-sm-6">
    {!! Form::label('location_id', 'Associate Location:') !!}
    <select class="form-control" id="location_id" name="location_id">
        <option value="0" disabled>Select Location</option>
        @foreach($locations as $location)
            <option value='{{$location["id"]}}' @if(isset($user) && $user && isset($user->location_id) && $user->location_id == $location["id"]) selected @endif)>{{$location["name"]}}</option>
        @endforeach
    </select>
</div>

{{-- @push('scripts')
    <script type="text/javascript">
        $('#email_verified_at').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: false
        })
    </script>
@endpush --}}

<!-- Password Field -->
<div class="form-group col-sm-6">
    {!! Form::label('password', 'Password:') !!}
    {!! Form::password('password', ['class' => 'form-control']) !!}
</div>

{{-- <!-- Remember Token Field -->
<div class="form-group col-sm-6">
    {!! Form::label('remember_token', 'Remember Token:') !!}
    {!! Form::text('remember_token', null, ['class' => 'form-control']) !!}
</div> --}}

{{-- <!-- Store Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('store_url', 'Store Url:') !!}
    {!! Form::text('store_url', null, ['class' => 'form-control']) !!}
</div> --}}

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('users.index') }}" class="btn btn-default">Cancel</a>
</div>
