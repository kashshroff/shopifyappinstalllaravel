<aside class="main-sidebar" id="sidebar-wrapper" style="padding:25% 2%"">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar" style="padding: 5% 2%; border: 1px solid #f4f4f4">

        
        <!-- Sidebar Menu -->

        <ul class="sidebar-menu" data-widget="tree">
            @include('layouts.menu')
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>