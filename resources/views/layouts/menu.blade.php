@role('admin')
<li class="{{ Request::is('configs*') ? 'active' : '' }}">
    <a href="{{ route('configs.index') }}"><i class="fa fa-edit"></i><span>Configs</span></a>
</li>
@endrole

<style>
    .nav-pills .nav-link.active, .nav-pills .show>.nav-link{
        background-color: #fff !important;
        font-weight: bold;
    }
</style>