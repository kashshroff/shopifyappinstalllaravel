<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RedactRequest extends Model
{
    protected $table = 'redact_request';
    protected $fillable = ["type", "details"];

    protected $casts = [
        "details" => "array"
    ];
}
