<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShopDetail extends Model
{
    //

    protected $casts = [
        "store_details" => "array",
        "theme_data"    => "array"
    ];
}
