<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\Model as EloquentModel;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Config
 * @package App\Models
 * @version February 24, 2020, 9:19 am UTC
 *
 * @property string name
 * @property string type
 * @property string configuration
 * @property string status
 */
class Config extends EloquentModel
{
    use SoftDeletes;

    public $table = 'configs';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'type',
        'configuration',
        'status'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'type' => 'string',
        'configuration' => 'string',
        'status' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'type' => 'required',
        'configuration' => 'required'
    ];

    
}
