<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Integration extends Model
{
    public $fillable = [
        'user_id',
        'integration_type',
        'config',
        'status'
    ];

    protected $casts = [
        "config" => "array"
    ];
}
