<?php

namespace App\Repositories;

use App\Models\Config;
use App\Repositories\BaseRepository;

/**
 * Class ConfigRepository
 * @package App\Repositories
 * @version February 24, 2020, 9:19 am UTC
*/

class ConfigRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'type',
        'configuration',
        'status'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Config::class;
    }
}
