<?php
namespace App\Helpers;
use App\Models\ShopDetail;
use App\Models\Config;
use App\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use PDO;

class DBHelpers
{
    public function getStoreDetails($shopUrl = null){
        if(!$shopUrl){
            $shopUrl = $this->getCurrentUser()->store_url;
        }
        try{
            $shop = ShopDetail::where([['shop_url', '=', $shopUrl]])->first();  
            if($shop){
                return [
                    "error" => 0,
                    "data" => $shop
                ];
            }else {
                return [
                    "error" => 1,
                    "message" => "Shop not present with ".env('APP_NAME')
                ];
            }
        } catch(Exception $ex){
            return [
                "error" => 1,
                "message" => "Shop not present with ".env('APP_NAME')
            ];
        }
    }

    public function getCurrentUser(){
        return Auth::user();
    }

    public function getAccessToken($shopUrl=null){
        
        if(!$shopUrl){
            $shopUrl = $this->getCurrentUser()->store_url;
        }
        
        // Get App-Key for this
        $storeData = $this->getStoreDetails($shopUrl);

        if(!$storeData['error']){
            return [
                "error"    => 0,
                "storeUrl" => $shopUrl,
                "accessToken" => $storeData['data']->access_token
            ];
        }else {
            return [
                "error" => 1,
                "message" => $storeData
            ];
        }
    }

    public function postDataToShopify($endpoint, $dataToPostToShopify, $storeUrl=null){
        $data = $this->getAccessToken($storeUrl);
        $accessToken = $data['accessToken'];
        $currentUserStore = $data['storeUrl'];
        
        // Get All Locations
        $shopify = new ShopifyHelpers($currentUserStore, $accessToken);
        $data = $shopify->postDataToShopify($endpoint, $dataToPostToShopify);
        dd($data);
        return $data;
    }

    public function getConfiguration($name){
        try{
            $config = Config::where([["name", "=", $name]])->first();
            if($config){
                return [
                    "error" => 0,
                    "data" => $config
                ];
            }else {
                return [
                    "error" => 1,
                    "message" => "Config not present with ".env('APP_NAME')
                ];
            }
        } catch(Exception $ex){
            return [
                "error" => 1,
                "message" => "Config not present with ".env('APP_NAME')
            ];
        }
    }

}