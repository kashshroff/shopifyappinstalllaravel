<?php
namespace App\Helpers;
use App\ShopDetail;
use phpish\shopify as phpishShopify;
use Oseintow\Shopify\Facades\Shopify;
use Illuminate\Support\Facades\Log;
use App\Models\Config;
use Exception;

class ShopifyHelpers
{
    protected $accessToken;
    protected $shopUrl;
    protected $shopify;
    protected $shopifyAppKey;
    protected $orders;
    private $_privateApp;

    public function __construct($shopUrl, $accessToken, $privateApp = false)
    {
        $this->accessToken = $accessToken;
        $this->shopUrl =  $shopUrl;
        $this->shopifyAppKey = config('shopify.key');
        $this->_privateApp = $privateApp;
    }

    public function createUninstallWebhook ()
    {
        try {
            $this->createWebhook("app/uninstalled", config('shopify.uninstall_webhook_address'));
            return true;
        } catch (\Exception $e) {
            Log::info($e->getMessage()." Error while creating Uninstall Webhook");
            return false;
        }
    }

    public function createWebhook($topic, $script_path)
    {
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            //$endpoint = "/admin/api/2019-07/webhooks.json";
            $endpoint = $this->getAPIEndpoint('webhooks');
            $webhook = $shopify("POST " . $endpoint, array(), array(
                'webhook' => array(
                    'topic' => $topic,
                    'address' => $script_path,
                    'format' => 'json'
                )
            ));
            return $webhook;
        } catch (\Exception $e) {
            Log::info($e->getMessage()." Error while creating Webhook");
            return;
        }
    }

    public function getDataFromShopify($endpoint){
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            $data = $shopify("GET " . $endpoint);
            return $data;
        } catch (\Exception $e) {
            Log::info($e->getMessage()." Error while getting data from shopify");
            return ["error" => 1, "message" => $e->getMessage()];
        }
    }

    public function insertEmptyTheme() {
        Log::info("Inserting Empty Theme with name ".env('THEME_NAME'));
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            //$endpoint = "/admin/api/2020-01/script_tags.json";
            
            $endpoint = $this->getAPIEndpoint('themes');
            Log::info("Endpoint $endpoint \n\n\n\n\n");
            $theme = $shopify("POST " . $endpoint, array(), array(
                'theme' => array(
                    "name" => env('THEME_NAME'),
                    "src" => 'https://plobalapps.s3-ap-southeast-1.amazonaws.com/shopify-empty-theme.zip',
                    "role" => "unpublished"
                )
            ));

            Log::info($theme['id']." theme id");
            return $theme;
        } catch (\Exception $e) {
            Log::error($e->getMessage()." Error while adding theme to stores");
            //dd($e->getMessage()); 
        }
    }

    // public function insertSnippit(){
    //     try {
    //         $themeData = [];
    //         $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
    //         //$endpoint = "/admin/api/2020-01/script_tags.json";
    //         Log::info("Theme name : ".config('shopify.theme_name'));
    //         $endpoint = $this->getAPIEndpoint('themes');
    //         $theme = $shopify("POST " . $endpoint, array(), array(
    //             'theme' => array(
    //                 "name" => (config('shopify.theme_name')),
    //                 "src" => 'https://plobalapps.s3-ap-southeast-1.amazonaws.com/shopify-empty-theme.zip',
    //                 "role" => "unpublished"
    //             )
    //         ));
    //         $themeData['theme'] = $theme;
    //         Log::info($theme['id']." theme id");

    //         $endpoint_assets = $this->getAPIEndpoint('assets');
    //         $endpoint_assets = $this->replaceMacroFromString(["**themeId**" => $theme['id']], $endpoint_assets);
                
    //         // Get File contents of JS File
    //         $js = file_get_contents((env('API_BASE_URL').'/js/etheme.js'));
    //         $js = str_replace("**API_BASE_URL**",env('API_BASE_URL'),$js);

    //         Log::info("JS File : ", [$js]);

    //         $asset = $shopify("PUT " . $endpoint_assets, '', array(
    //             'asset' => array(
    //                 "key" => 'assets/etheme.js',
    //                 "value" => $js
    //             )
    //         ));
    //         $themeData['asset'] = $asset;

    //         // $endpoint_script = $this->getAPIEndpoint('script_tags');
    //         // $script = $shopify("POST " . $endpoint_script, array(), array(
    //         //     'script_tag' => array(
    //         //         "src" => $asset['public_url'],
    //         //         "event" => "onload",
    //         //         "display_scope" => "all"
    //         //     )
    //         // ));
    //         // Log::info($asset['public_url']." assets public_url");

    //         // $themeData['script'] = $script;

    //         if(env('APP_ENV') == 'local'){
    //             $endpoint_script = $this->getAPIEndpoint('script_tags');
    //             $script = $shopify("POST " . $endpoint_script, array(), array(
    //                 'script_tag' => array(
    //                     "src" => env('API_BASE_URL').'/js/etheme.js',
    //                     "event" => "onload",
    //                     "display_scope" => "all"
    //                 )
    //             ));
    //             Log::info("Local assets public_url");
    //             $themeData['local_script'] = $script;
    //         }

    //         return $themeData;
    //     } catch (\Exception $e) {
    //         Log::info($e->getMessage()." Error while adding snippit to stores scripts");
    //         //dd($e->getMessage()); 
    //     }
    // }

    public function insertSnippit($theme){
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);

            Log::error($theme['id']." theme id");

            $endpoint_assets = $this->getAPIEndpoint('assets');
            $endpoint_assets = $this->replaceMacroFromString(["**themeId**" => $theme['id']], $endpoint_assets);
                
            $js = file_get_contents((env('API_BASE_URL').'/js/etheme.js'));
            $js = str_replace("**API_BASE_URL**",env('API_BASE_URL'),$js);

            Log::info("JS File : ", [$js]);

            $asset = $shopify("PUT " . $endpoint_assets, '', array(
                'asset' => array(
                    "key" => 'assets/etheme.js',
                    "value" => $js
                )
            ));
            
            $endpoint_script = $this->getAPIEndpoint('script_tags');
            $scripts = $shopify("GET " . $endpoint_script, [], []); 
            if($scripts && count($scripts)) {
                foreach ($scripts as $script) {
                    $endpoint_script = $this->getAPIEndpoint('script_tags');
                    $endpoint_script = str_replace(".json", "/".$script["id"].".json", $endpoint_script);
                    Log::error(" endpoint_script ", [$endpoint_script]);
                    $scripts = $shopify("DELETE " . $endpoint_script, [], []); 
                }
            }
            if(env('APP_ENV') == 'local'){
                Log::error(" assets ", [$asset]);
                $endpoint_script = $this->getAPIEndpoint('script_tags');
                $script = $shopify("POST " . $endpoint_script, array(), array(
                    'script_tag' => array(
                        "src" => env('API_BASE_URL').'/js/etheme.js',
                        "event" => "onload",
                        "display_scope" => "all"
                    )
                ));
            Log::error("LOCAL assets public_url script", [$script]);
            }else {
                Log::error(" assets ", [$asset]);
                $endpoint_script = $this->getAPIEndpoint('script_tags');
                $script = $shopify("POST " . $endpoint_script, array(), array(
                    'script_tag' => array(
                        "src" => $asset['public_url'],
                        "event" => "onload",
                        "display_scope" => "all"
                    )
                ));
                Log::error(" assets public_url script", [$script]);
            }

            return true;
        } catch (\Exception $e) {
            Log::error($e->getMessage()." Error while adding snippit to stores scripts");
            //dd($e->getMessage()); 
        }
    }

    function deleteSnippit(){
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            $res = $shopify->call([
                'URL' => '/admin/script_tags.json',
                'METHOD' => 'GET'
            ]);
            
            if (count($res->script_tags)) {
                foreach ($res->script_tags as $scripts) {
                    if ($scripts->src && strpos($scripts->src, 'etheme.js') !== false) { 
                        $res = $shopify->call([
                            'URL' => '/admin/script_tags/' . $scripts->id . '.json',
                            'METHOD' => 'DELETE'
                        ]);
                    }
                }
            }
            return;
        } catch (\Exception $e) {
            Log::info($e->getMessage()." Error while deleting snippit from stores scripts");
            //dd($e->getMessage()); 
        }
    }

    function deleteTheme($themeId){
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        try {
            $endpoint_theme = $this->getAPIEndpoint('theme');
            $endpoint_theme = $this->replaceMacroFromString(["**id**" => $themeId], $endpoint_theme);
            Log::info("Deleting Endpoint : $endpoint_theme");
            $theme = $shopify("DELETE " . $endpoint_theme, array(), array());
        } catch (\Exception $e) {
            Log::info($e->getMessage()." Error while deleting Theme from stores scripts");
            //dd($e->getMessage()); 
        }
    }

    public function postDataToShopify($endpoint, $data){
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            $data = $shopify("POST " . $endpoint, array() , $data);
            return $data;
        } catch (\Exception $e) {
            Log::info($e->getMessage()." Error while posting data to shopify");
            return ["error" => 1, "message" => $e->getMessage()];
        }
    }

    public static function getAPIEndpoint($apiRequset){
        $config = Config::where('name','api/end_points')->first();
        $conf = json_decode($config['configuration']);
        return ($conf->{$apiRequset});
    }

    public static function replaceMacroFromString($replaceArray, $stringToReplace){
        $returnPath = $stringToReplace;
        foreach ($replaceArray as $key => $val) {
            if (strpos($returnPath, $key)) {
                $returnPath = str_replace($key, $val, $returnPath);
            }
        }
        return $returnPath;
    }

    public function insertMetafields($customerId, $data){
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        
        Log::info("Inserting Metafields......");
        
        $endpoint = $this->getAPIEndpoint('metafield_customer');
        $endpoint = $this->replaceMacroFromString(["**customerId**" => $customerId], $endpoint);
        try{
            $metafield = $shopify("POST " . $endpoint, array(), array(
                'metafield' => array(
                    "namespace" => 'cart_sync',
                    "key" => "products",
                    "value" => json_encode($data),
                    "value_type" => "json_string"
                )
            ));
            return $metafield;
        } catch (Exception $ex){
            Log::error("Error while updating theme data ", [$ex->getMessage()]);
            return ["error" => 1, "message" => $ex->getMessage()];
        }
    }

    public function updateMetafields($metafieldId, $data){
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        
        
        $endpoint = $this->getAPIEndpoint('metafield');
        $endpoint = $this->replaceMacroFromString(["**id**" => $metafieldId], $endpoint);
        try{
            $metafield = $shopify("PUT " . $endpoint, array(), array(
                'metafield' => array(
                    "id" => $metafieldId,
                    "value" => json_encode($data),
                    "value_type" => "json_string"
                )
            ));
            Log::info("Metafield Updated..........", [$metafield]);
            
            return $metafield;
        } catch (Exception $ex){
            return ["error" => 1, "message" => $ex->getMessage()];
            Log::error("Error while updating theme data ", [$ex->getMessage()]);
        }
    }


    public function deleteMetafields($metafieldId){
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        
        
        $endpoint = $this->getAPIEndpoint('metafield');
        $endpoint = $this->replaceMacroFromString(["**id**" => $metafieldId], $endpoint);
        try{
            $metafield = $shopify("DELETE " . $endpoint, array(), array());
            Log::info("Metafield Deleted..........");
            return $metafield;
        } catch (Exception $ex){
            return ["error" => 1, "message" => $ex->getMessage()];
            Log::error("Error while deleting Metafield ", [$ex->getMessage()]);
        }
    }

    public function getMetafields($customerId){
        try{
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            
            Log::info("Getting Metafields......");
            
            $endpoint = $this->getAPIEndpoint('metafield_customer');
            $endpoint = $this->replaceMacroFromString(["**customerId**" => $customerId], $endpoint);
            $endpoint .= '?namespace=cart_sync';
            $metafields = $shopify("GET " . $endpoint, array(), array());

            Log::info("Metafield Get .............", [$metafields]);
            return $metafields;
        }catch (Exception $ex){
            return ["error" => 1, "message" => $ex->getMessage()];
        }
    }

    public function themeExists($themeID) {
        $return = false; 
        try {
            $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
            Log::info($themeID." theme id");
            $endpoint_assets = $this->getAPIEndpoint('assets');
            $endpoint_assets = $this->replaceMacroFromString(["**themeId**" => $themeID], $endpoint_assets);
                
            $asset = $shopify("GET " . $endpoint_assets, '', []);
            Log::info("assets", [$asset]);
            $return = true; 
        } catch (\Exception $e) {
            return ["error" => 1, "message" => $ex->getMessage()];
            Log::error($e->getMessage()." Error while adding theme to stores");
        }
        return $return;
    }

    public function getCurrentTheme(){
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        
        Log::info("Getting Current Theme......");
        try{
            $endpoint = $this->getAPIEndpoint('themes');
            $endpoint .= '?role=main';
            $theme = $shopify("GET " . $endpoint, array(), array());

            Log::info("Current Theme Get .............", [$theme]);
            return $theme;
        } catch (Exception $ex){
            return ["error" => 1, "message" => $ex->getMessage()];
            Log::info("Erroe Current Theme Get .............", [$ex->getMessage()]);
        }
    }

    public function getThemeAsset($themeId, $asset) {
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        
        Log::info("Getting getThemeAsset......");
        try{
            $endpoint = $this->getAPIEndpoint('assets');
            $endpoint = $this->replaceMacroFromString(["**themeId**" => $themeId], $endpoint);
            $endpoint .= '?asset[key]='.$asset;
            $theme = $shopify("GET " . $endpoint, array(), array());

            Log::info("getThemeAsset Get .............", [$theme]);
            return $theme;
        } catch (Exception $ex){
            return ["error" => 1, "message" => $ex->getMessage()];
            Log::info("Erroe getThemeAsset Get .............", [$ex->getMessage()]);
        }
    }

    public function updateThemeAsset($themeId, $assetKey, $assetValue) {
        $shopify = phpishShopify\client($this->shopUrl, $this->shopifyAppKey, $this->accessToken);
        
        Log::info("Getting updateThemeAsset......");
        try{
            $endpoint = $this->getAPIEndpoint('assets');
            $endpoint = $this->replaceMacroFromString(["**themeId**" => $themeId], $endpoint);
            $theme = $shopify("PUT " . $endpoint, array(), array(
                "asset"=> [
                    "key"=> $assetKey,
                    "value"=> $assetValue
                ]
            ));

            Log::info("updateThemeAsset Get .............", [$theme]);
            return $theme;
        } catch (Exception $ex){
            return ["error" => 1, "message" => $ex->getMessage()];
            Log::info("Erroe updateThemeAsset Get .............", [$ex->getMessage()]);
        }
    }
    
}

?>