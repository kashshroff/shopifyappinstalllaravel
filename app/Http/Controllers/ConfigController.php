<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateConfigRequest;
use App\Http\Requests\UpdateConfigRequest;
use App\Repositories\ConfigRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class ConfigController extends AppBaseController
{
    /** @var  ConfigRepository */
    private $configRepository;

    public function __construct(ConfigRepository $configRepo)
    {
        $this->configRepository = $configRepo;
    }

    /**
     * Display a listing of the Config.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $configs = $this->configRepository->paginate(10);
        
        return view('configs.index')
            ->with('configs', $configs);
    }

    /**
     * Show the form for creating a new Config.
     *
     * @return Response
     */
    public function create()
    {
        return view('configs.create');
    }

    /**
     * Store a newly created Config in storage.
     *
     * @param CreateConfigRequest $request
     *
     * @return Response
     */
    public function store(CreateConfigRequest $request)
    {
        $input = $request->all();

        if($input['status'] == 1){
            $input['status'] = 'ACTIVE';
        }else {
            $input['status'] = 'INACTIVE';
        }

        $config = $this->configRepository->create($input);

        Flash::success('Config saved successfully.');

        return redirect(route('configs.index'));
    }

    /**
     * Display the specified Config.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $config = $this->configRepository->find($id);
        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('configs.index'));
        }

        return view('configs.show')->with('config', $config);
    }

    /**
     * Show the form for editing the specified Config.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $config = $this->configRepository->find($id);

        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('configs.index'));
        }

        return view('configs.edit')->with('config', $config);
    }

    /**
     * Update the specified Config in storage.
     *
     * @param int $id
     * @param UpdateConfigRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConfigRequest $request)
    {
        $config = $this->configRepository->find($id);

        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('configs.index'));
        }

        $config = $this->configRepository->update($request->all(), $id);

        Flash::success('Config updated successfully.');

        return redirect(route('configs.index'));
    }

    /**
     * Remove the specified Config from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $config = $this->configRepository->find($id);

        if (empty($config)) {
            Flash::error('Config not found');

            return redirect(route('configs.index'));
        }

        $this->configRepository->delete($id);

        Flash::success('Config deleted successfully.');

        return redirect(route('configs.index'));
    }
}
