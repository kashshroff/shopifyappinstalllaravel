<?php

namespace App\Http\Controllers;

use App\Models\RedactRequest;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class RedactController extends Controller {
    public function customers(Request $request){
        Log::info("Redact Customers Request", $request->all());
        $type = 'customer';
        try{
            $db = new RedactRequest();
            $db->type = $type;
            $db->details = $request->all();
            $db->save();
        } catch (Exception $ex){
            Log::error("Error in Redact DB insertion", [$ex->getMessage()]);
        }
        
    }

    public function shop(Request $request){
        Log::info("Redact shop Request", $request->all());
        $type = 'shop';

        try{
            $db = new RedactRequest();
            $db->type = $type;
            $db->details = $request->all();
            $db->save();
        } catch (Exception $ex){
            Log::error("Error in Redact DB insertion", [$ex->getMessage()]);
        }
    }

    public function dataRequest(Request $request){
        Log::info("Redact dataRequest Request", $request->all());
        $type = 'data_request';

        try{
            $db = new RedactRequest();
            $db->type = $type;
            $db->details = $request->all();
            $db->save();
        } catch (Exception $ex){
            Log::error("Error in Redact DB insertion", [$ex->getMessage()]);
        }
    }
}