<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreateConfigAPIRequest;
use App\Http\Requests\API\UpdateConfigAPIRequest;
use App\Models\Config;
use App\Repositories\ConfigRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController;
use Response;

/**
 * Class ConfigController
 * @package App\Http\Controllers\API
 */

class ConfigAPIController extends AppBaseController
{
    /** @var  ConfigRepository */
    private $configRepository;

    public function __construct(ConfigRepository $configRepo)
    {
        $this->configRepository = $configRepo;
    }

    /**
     * Display a listing of the Config.
     * GET|HEAD /configs
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $configs = $this->configRepository->all(
            $request->except(['skip', 'limit']),
            $request->get('skip'),
            $request->get('limit')
        );

        return $this->sendResponse($configs->toArray(), 'Configs retrieved successfully');
    }

    /**
     * Store a newly created Config in storage.
     * POST /configs
     *
     * @param CreateConfigAPIRequest $request
     *
     * @return Response
     */
    public function store(CreateConfigAPIRequest $request)
    {
        $input = $request->all();

        $config = $this->configRepository->create($input);

        return $this->sendResponse($config->toArray(), 'Config saved successfully');
    }

    /**
     * Display the specified Config.
     * GET|HEAD /configs/{id}
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Config $config */
        $config = $this->configRepository->find($id);

        if (empty($config)) {
            return $this->sendError('Config not found');
        }

        return $this->sendResponse($config->toArray(), 'Config retrieved successfully');
    }

    /**
     * Update the specified Config in storage.
     * PUT/PATCH /configs/{id}
     *
     * @param int $id
     * @param UpdateConfigAPIRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateConfigAPIRequest $request)
    {
        $input = $request->all();

        /** @var Config $config */
        $config = $this->configRepository->find($id);

        if (empty($config)) {
            return $this->sendError('Config not found');
        }

        $config = $this->configRepository->update($input, $id);

        return $this->sendResponse($config->toArray(), 'Config updated successfully');
    }

    /**
     * Remove the specified Config from storage.
     * DELETE /configs/{id}
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Config $config */
        $config = $this->configRepository->find($id);

        if (empty($config)) {
            return $this->sendError('Config not found');
        }

        $config->delete();

        return $this->sendSuccess('Config deleted successfully');
    }
}
