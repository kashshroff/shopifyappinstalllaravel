<?php

namespace App\Http\Controllers;

use App\Helpers\DBHelpers;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use App\Http\Controllers\AppBaseController;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use Flash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Response;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        // $users = $this->userRepository->all();

        $users = User::where([['store_url', '=', Auth::user()->store_url]])->paginate(10);
        
        // $dbHelper = new DBHelpers;
        // $locations = $dbHelper->getAllLocations();

        // foreach($users as $index => $user){
        //     foreach($locations as $location){
        //         if($user->location_id == $location['id']){
        //             $users[$index]->location_id = $location['name'];
        //             break;
        //         }
        //     }
        // }
        
        return view('users.index')
            ->with('users', $users);
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $dbHelper = new DBHelpers;
        $locations = $dbHelper->getAllLocations();
        
        return view('users.create')->with('locations', $locations);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $dbHelper = new DBHelpers;
        $storeAdmin = $dbHelper->getCurrentUser();
        $input['store_url'] = $storeAdmin->store_url;
        
        $input['password'] = Hash::make($input['password']);

        $user = $this->userRepository->create($input);

        $storeAssociateRole = Role::where([['name', '=', 'store_associate']])->first();
        
        $user->attachRole($storeAssociateRole);

        Flash::success('User saved successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.show')->with('user', $user);
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);
        $dbHelper = new DBHelpers;
        $locations = $dbHelper->getAllLocations();

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        return view('users.edit')->with('user', $user)->with('locations', $locations);
    }

    /**
     * Update the specified User in storage.
     *
     * @param int $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->find($id);
        
        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }
        $input = $request->all();
        
        $input['password'] = Hash::make($input['password']);
        $user = $this->userRepository->update($input, $id);
        // dd($user);
        Flash::success('User updated successfully.');

        return redirect(route('users.index'));
    }

    /**
     * Remove the specified User from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Flash::error('User not found');

            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Flash::success('User deleted successfully.');

        return redirect(route('users.index'));
    }
}
