<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Oseintow\Shopify\Facades\Shopify;
use App\Models\ShopDetail;
use App\Helpers\ShopifyHelpers as ShopifyHelper;
use App\Helpers\DBHelpers;
use App\Role;
use Exception;
use Illuminate\Support\Facades\Log;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laratrust\Laratrust;

class ShopifyController extends Controller
{
    protected $accessToken = '';


    public function install(Request $request){
        $shopUrl = $request->shop;
        
        $scope = config('shopify.scopes');
        $redirectUrl = config('shopify.redirect_uri');
        $shopify = Shopify::setShopUrl($shopUrl);
        //dd($redirectUrl);
        return redirect()->to($shopify->getAuthorizeUrl($scope,$redirectUrl));
    }

    public function auth(Request $request){
        Log::info("Authenticating $request->shop...........");
        $shopUrl = $request->shop;
        $user = null;
        $themeId= null;
        
        $this->accessToken = Shopify::setShopUrl($shopUrl)->getAccessToken($request->code);
        Log::info("AccessToken", [$this->accessToken]);
        try{
            $shopify = Shopify::setShopUrl($shopUrl)->setAccessToken($this->accessToken);
            $storeDetails = $shopify->get("admin/shop.json");
        } catch (Exception $ex){
            $storeDetails = [];
        }
        Log::info("Shop Details", [$storeDetails]);
        // Check if store is already present in env('APP_NAME') database
        $dbHelper = new DBHelpers;
        $shop = $dbHelper->getStoreDetails($shopUrl);
        Log::info("Database Shop", [$shop]);

        $shopifyHelper = new ShopifyHelper($shopUrl, $this->accessToken);

        $themeData = [];

        // If Shop is present then check if user is present and is same.
        if(!$shop['error']){
            /**
             * Check if user exists
             * of current store
             * in env('APP_NAME') DB
             */

            $shop = $shop['data'];
            
            /**
             * Check if store is inactive
             * If yes
             * Reinstall Case
             */
            if($shop->status == 'INACTIVE'){
                Log::info("Reinstalling ...............");
                $themeId = $shop->theme_data['theme']['id'];
                $shop->status = 'ACTIVE';
                //Insert JS Snippit
                try{
                    // Check if previously added theme is present.
                    $themeId = $shop->theme_id;
                    if(!isset($themeId) || !$shopifyHelper->themeExists($themeId)) {
                        $themeDetails = $shopifyHelper->insertEmptyTheme();
                        $themeData['theme'] = $themeDetails;
                        $shop->theme_data = $themeData;
                    }

                    
                    // Add Uninstall Webhook\
                    $uninstallWebhook = $shopifyHelper->createUninstallWebhook();

                    // Add Orders Webhook
                    // $orderWebhookUrl = str_replace("**SHOP**",$shopUrl,env('ORDERS_WEBHOOK'));
                    // $ordersWebhook = $shopifyHelper->createWebhook('orders/create', $orderWebhookUrl);
                    // Log::info("Orders Webhook Created", [$ordersWebhook]);

                } catch (Exception $ex){
                    Log::info("Error in inserting snippet ...............", [$ex->getMessage()]);
                }
            }else {
                Log::info("Normal Install, already a customer. Showing its themeData", [$themeData]);
                $themeData = $shop->theme_data;
            }

            //  Current Store User Email
            $currentUserEmail = $storeDetails['email'];
            
            // User in plobal DB in shop_details
            $inShopUser = $shop->store_details['email'];

            $user = User::where([['email', '=', $inShopUser], ['store_url', '=', $shopUrl]])->first();

            if($currentUserEmail != $inShopUser){
                // Update Admin user table entry for admin
                $user->email = $currentUserEmail;
                $user->save();
            }

            $tokenResult = $user->createToken('Store Access Token');
            $token = $tokenResult->token;
            Log::info("TOKEN", [$tokenResult]);
            $token->save();
            $shop->mobile_token = $tokenResult->accessToken;
            
        }
        else{
            Log::info("NEW SHOP");
            // User Creation
            $user = new User;
            $user->name = $storeDetails['name'];
            $user->email = $storeDetails['email'];
            $user->password = Hash::make('asdasd');
            $user->store_url = $storeDetails['domain'];

            $user->save();

            $tokenResult = $user->createToken('Store Access Token');
            $token = $tokenResult->token;

            Log::info("TOKEN", [$tokenResult]);
            $token->save();
            
            Log::info("New User", [$user]);
            $storeAdminRole = Role::where([['name', '=', 'store_admin']])->first();
            $user->attachRole($storeAdminRole);
            Log::info("Role Details", [$user]);
            
            /**
             * Uncomment this function
             * to insert any snippet
             */
            // $themeData['theme'] = $shopifyHelper->insertEmptyTheme();

            // Add Uninstall Webhook
            $uninstallWebhook = $shopifyHelper->createUninstallWebhook();

            // Add Orders Webhook
            // $orderWebhookUrl = str_replace("**SHOP**",$shopUrl,env('ORDERS_WEBHOOK'));
            // $ordersWebhook = $shopifyHelper->createWebhook('orders/create', $orderWebhookUrl);
            // Log::info("Orders Webhook Created", [$ordersWebhook]);

            $shop = new ShopDetail;

            $shop->mobile_token = $tokenResult->accessToken;
            $shop->theme_data = $themeData;
        }

        /**
         * Uncomment this function
         * to insert any snippet
         */

        // // Get Existing theme id
        // $mainTheme = $shopifyHelper->getCurrentTheme();
        // $currentThemeId = $mainTheme[0]['id'];

        // // Update layout/theme.liquid asset with custom data
        // $currentAsset = $shopifyHelper->getThemeAsset($currentThemeId, 'layout/theme.liquid');

        // $config = $dbHelper->getConfiguration('theme_script');
        // $config = $config['data'];

        // // Comparing if our code is already present in Main theme or not.
        // if(strpos($currentAsset['value'], $config->configuration) !== false){
        //     Log::info("String comparision result : Found");
        // } else{
        //     Log::info("String comparision result : NotFound");
        //     $newAsset = $currentAsset['value'].$config->configuration;
        //     Log::info($newAsset);
            
        //     $updatedAsset = $shopifyHelper->updateThemeAsset($currentThemeId, 'layout/theme.liquid', $newAsset);
        // }
        // $shopifyHelper->insertSnippit($themeData['theme']);

        // Update or Add new Shop Table
        $shop->shop_url = $shopUrl;
        $shop->access_token = $this->accessToken;
        $shop->store_details = $storeDetails;

        
        
        $shop->save();

        Log::info("Auth process completed");
        
        
        Auth::loginUsingId($user->id);
        return redirect()->to('/home');
    }

    public function success($shop, $user){

        Auth::loginUsingId($user->id);
        return redirect("/home");
        
    }

    public function uninstall(Request $request){
        
        Log::info($request->domain);
        $shop = ShopDetail::where("shop_url", "=", $request->domain)->first();

        $shop->status = "INACTIVE";
        $shop->save();

        Log::info($request->domain." has been marked Uninstalled");
    }


}