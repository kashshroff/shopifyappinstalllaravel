## Laravel - Shopify Boilerplate

1)Get API key and Secret key from shopify
2)Install Url should be https://www.yourdomain.test/install
3)Auth Url should be https://www.yourdomain.test/auth
4)Uninstall Url should be https://www.yourdomain.test/uninstall

Steps:

1) Clone the repo inside docker / XAMPP / Server etc...
2) composer update
3) Change .env.example to .env and set you database connection in it
4) php artisan migrate
5) In config/shopify.php , update the shopify variables

## Customer Install URL

https://www.yourdomain.test/install?shop=CUSTOMER_MYSHOPIFY_ADDRESS

## For More Infomation

Check app/Http/Controllers/ShopifyController.php