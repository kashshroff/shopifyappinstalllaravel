<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Config;
use Faker\Generator as Faker;

$factory->define(Config::class, function (Faker $faker) {

    return [
        'name' => $faker->word,
        'type' => $faker->word,
        'configuration' => $faker->text,
        'status' => $faker->randomElement(['1']),
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
