<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id'   => 1,
                'name' => "store_admin",
                'display_name' => "Store Admin",
                'description' => "Store Admin Role",
            ],
            [
                'id'   => 2,
                'name' => "admin",
                'display_name' => "Admin",
                'description' => "Admin Role",
            ],
            [
                'id'   => 3,
                'name' => "store_associate",
                'display_name' => "Store Associate",
                'description' => "Store Associate Role",
            ]
        ];
        DB::table('roles')->insert($roles);
    }
}
