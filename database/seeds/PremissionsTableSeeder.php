<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PremissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            [
                'id'   => 1,
                'name' => "create_user",
                'display_name' => "Create User",
                'description' => "Create Staff or Users for dashboard",
            ],
        ];
        DB::table('permissions')->insert($permissions);
    }
}
