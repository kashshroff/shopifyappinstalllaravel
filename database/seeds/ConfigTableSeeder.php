<?php

use App\Models\Config;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ConfigTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configs = [
            [
                'configuration' => "<script>
                var CartSyncCustomer={};
                var CartSyncCurrentCart={};
                {%assign cart_sync=customer.metafields.cart_sync%}
                {%assign key='products'%}
                {%if customer%}
                CartSyncCustomer={
                  isLoggedIn:!0,
                  customer_id:{{customer.id}},
                  in_metafields_products:{{customer.metafields.cart_sync.products|t|json}}
                };
                CartSyncCurrentCart={
                  cart:{{cart|t|json}}
                };
                {%endif%}
              </script>",
                'name' => "theme_script",
                'type' => "script",
                'status' => 'ACTIVE'
            ],
            [
                'configuration' => '{
                    "partner_handle": "plobalapps-mobile-application",	
                    "secret_key": "plobalapps@PushOwl-SGHR19Q"
                   }',
                'name' => "pushowl",
                'type' => "JSON",
                
                'status' => 'ACTIVE'
            ]
        ];

        foreach($configs as $config){
            $user = Config::create($config);
        }

        

        // DB::table('configs')->insert($configs);
    }
}
