<?php

use App\User;
use App\Role;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $user = User::create([
            'name' => 'Admin',
            'email' => 'kash@gmail.com',
            'password' => Hash::make('asdasd'),
        ]);
        $role = Role::where([['name', '=', 'admin']])->firstOrFail();

        $user->attachRole($role);
    }
}
